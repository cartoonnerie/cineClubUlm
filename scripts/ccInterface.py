#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 25 18:20:49 2018

@author: alice
"""
import os

# Enhancements :
# * utiliser un dictionnaire pour les menus, afin d'avoir un menu de fond (quitter, corriger etc)
# * mettre en place une file d'instruction pour anticiper
# * vérifier les eval et les types donnés

from scripts.genCom import GenerateurComm
from scripts.cineclubBlogSQ import filmSQ as f
from config import migrations_db, db_path
import sqlite3

DEBUG = True


class FilmInterface():

    def __init__(self):
        self.filmCharge = None
        self.generateur_com = GenerateurComm()

    def menu(self):
        self.filmEnCours()
        champ = input(
            """Voulez-vous \n 1: consulter la liste des films de la base de données \n 2: ajouter un film à la base de données \n 3: afficher une communication\n 4: modifier une séance\n 5: créer une base de données vierges\n""")
        try:
            champ = eval(champ)
        except NameError:
            pass
        if champ == 1:
            self.selectFilm()
        elif champ == 2:
            self.newFilm()
            print("\nFilm enregistré.\n")
            self.menu()
        elif champ == 3:
            self.communication()
            pass
        elif champ == 4:
            self.corrigerFilm()
        elif champ == 5:
            self.initBDD()
        else:
            print("\n Champ inconnu. Pour quitter, appuyer sur Ctrl-D\n")
            self.menu()
        pass

    def filmEnCours(self):
        if self.filmCharge:
            print("\nLe film couramment chargé est", self.filmCharge.nom, "\n")

    def initBDD(self):
        instructions = []
        for filename in os.listdir(migrations_db):
            with open(os.path.join(migrations_db, filename), 'r') as fichier:
                instructions.append(fichier.read())

        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        for instruction in instructions:
            c.executescript(instruction)
        conn.commit()
        conn.close()

    def selectFilm(self):
        f.list_all()
        rep = input("Entrez le code identifiant du film\n")
        try:
            self.filmCharge = f(rep)
        except TypeError:
            print("L'identifiant n'est pas correct")
        finally:
            self.menu()

    def newFilm(self):
        self.filmCharge = f.new_film()

    def afficherFilm(self):
        if self.filmCharge:
            self.filmCharge.show()
        else:
            print("Vous n'avez aucun film chargé pour le moment.\n")
            rep = eval(input(
                "Tapez \n 1: Charger un film à partir de la base de données \n 2: ajouter un nouveau film à la base de données \n 0: revenir au menu principal \n"))
            if rep == 1:
                self.selectFilm()
            elif rep == 2:
                self.newFilm()
            elif rep == 0:
                self.menu()
            else:
                print("Votre entrée n'est pas valide. Merci d'entrer quelque chose de possible.\n")
                self.afficherFilm()

    def corrigerFilm(self):
        if self.filmCharge:
            # recuperer les cles possibles
            conn = sqlite3.connect(db_path)
            conn.row_factory = sqlite3.Row
            c = conn.cursor()
            try:
                c.execute("SELECT * FROM films")
                keys = [a[0] for a in c.description]

            except Exception as e:
                conn.rollback()
                if DEBUG:
                    raise (e)
            finally:
                conn.close()

            # faire la modification
            if keys:
                keys += ['acteurs']
                print("Les champs possibles sont : ")
                for k in keys:
                    print(k)
                nomAttr = input("Choisissez l'attribut que vous souhaitez modifier chez %s \n" % self.filmCharge.nom)
                if nomAttr == "acteurs":
                    print(
                        "La valeur actuelle du champ %s est %s.\nVeuillez entrer la nouvelle valeur pour ce champ (et -1 si vous ne souhaitez finalement pas modifier le champ)\n" % (
                        nomAttr, self.filmCharge.__getattr__(nomAttr)))
                    newValue = f.make_actors_list()
                elif nomAttr == "duree":
                    newValue = f.input_duration()
                elif nomAttr == "couleur":
                    newValue = f.input_color()
                elif nomAttr == "date":
                    newValue = f.input_date()
                else:
                    newValue = input(
                        "La valeur actuelle du champ %s est %s.\nVeuillez entrer la nouvelle valeur pour ce champ (et -1 si vous ne souhaitez finalement pas modifier le champ)\n" % (
                        nomAttr, self.filmCharge.__getattr__(nomAttr)))

                if newValue != "-1":
                    if nomAttr == "idN":
                        iStill = self.filmCharge.__getattr__(nomAttr)
                        self.filmCharge.__setattr__(nomAttr, newValue)
                        self.filmCharge = f(iStill)
                    else:
                        self.filmCharge.__setattr__(nomAttr, newValue)
                else:
                    print("\nVous n'avez finalement pas modifié cet attribut.")
            else:
                print("\nLa connexion avec la base de données a planté, la modification n'a pas été effectuée")

            # on retourne au reste de la navigation
            rep = input(
                "\nVoulez-vous\n 1: modifier une nouvelle fois le film %s \n 0: retourner au menu principal\n" % self.filmCharge.nom)
            if rep == "1":
                self.corrigerFilm()
            elif rep == "0":
                self.menu()

        else:
            print("\nVous n'avez pas de film chargé à corriger. Veuillez en sélectionner un")
            self.menu()

    def communication(self):
        self.filmEnCours()
        rep = input(
            "Voulez-vous afficher \n 1: l'article du bocal du film courant \n 2: l'article de blog du film courant \n 3: le calendrier de blog du film courant \n 4: la communication Facebook du film courant \n 5: l'email du film courant \n 6: le texte ics du film courant  \n 7: le petit bout de bocal du film courant \n 8: le calendrier des films à partir du film courant \n 9: changer le film sélectionné \n 0: revenir au menu principal \n")
        if rep == "1":
            print(self.generateur_com.bocal(self.filmCharge))
            self.finAction()
        elif rep == "2":
            print(self.generateur_com.blog(self.filmCharge))
            self.finAction()
        elif rep == "3":
            print(self.generateur_com.calendrier(self.filmCharge))
            self.finAction()
        elif rep == "4":
            print(self.generateur_com.facebook(self.filmCharge))
            self.finAction()
        elif rep == "5":
            print(self.generateur_com.mail(self.filmCharge))
            self.finAction()
        elif rep == "6":
            print(self.generateur_com.ics_text(self.filmCharge))
            self.finAction()
        elif rep == "7":
            print(self.generateur_com.bocal_small(self.filmCharge))
            self.finAction()
        elif rep == "8":
            print(self.generateur_com.allCalendars(self.filmCharge))
            self.finAction()
        elif rep == "9":
            self.selectFilm()
        elif rep == "0":
            self.menu()

    def finAction(self):
        self.menu()
        pass

    def main(self):
        self.menu()
        pass
