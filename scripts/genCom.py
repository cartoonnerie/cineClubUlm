from jinja2 import Environment, FileSystemLoader
from config import prices


def str_list(liste):
    return f'{", ".join(liste)}...'


def str_couleur(est_en_couleur):
    return 'Couleur' if est_en_couleur else 'Noir et blanc'


latex_jinja_env_params = {
    'block_start_string': '\\BLOCK{',
    'block_end_string': '}',
    'variable_start_string': '\\VAR{',
    'variable_end_string': '}',
    'comment_start_string': '\\#{',
    'comment_end_string': '}',
    'line_statement_prefix': '%%',
    'line_comment_prefix': '%#',
    'trim_blocks': True,
    'autoescape': False
}

filtres_jinja = {
    'strListe': str_list,
    'couleurStr': str_couleur
}

class GenerateurComm:
    def __init__(self):
        self.env_jinja = Environment(loader=FileSystemLoader('templates'))
        self.env_jinja.filters.update(filtres_jinja)

        self.latex_env_jinja = Environment(loader=FileSystemLoader('templates'), **latex_jinja_env_params)
        self.latex_env_jinja.filters.update(filtres_jinja)

    def build_com(self, film, nom_template, latex=False):
        env = self.latex_env_jinja if latex else self.env_jinja

        template = env.get_template(nom_template)
        out = template.render({'prices': prices, 'film': film})
        return out

    def bocal(self, film):
        return self.build_com(film, 'bocal.tex', latex=True)

    def bocal_small(self, film):
        return self.build_com(film, 'bocal_small.tex', latex=True)
    
    def blog(self, film):
        return self.build_com(film, 'blog.html.jinja2')

    def calendrier(self, film):
        return self.build_com(film, 'calendrier.html.jinja2')

    def facebook(self, film):
        return self.build_com(film, 'facebook.txt.jinja2')

    def mail(self, film):
        return self.build_com(film, 'mail.html.jinja2')

    def ics_text(self, film):
        return self.build_com(film, 'ics.md')

    def allCalendars(self, film):
        pass 
