{{ film.nom }} - {{ film.realisateur }}

**Pays** : {{ film.pays }}  
**Durée** : {{ film.duree }} minutes  
**Année** : {{ film.annee }}  
{{ film.couleur|couleurStr }}  
{{ film.formatCopie }} . {{ film.langST }}  
**Avec** : {{ film.acteurs|strListe }}  

{{ film.synopsis }}

Plus d'informations sur [notre site Internet](http://www.cineclub.ens.fr/category/seances/), [facebook](https://www.facebook.com/cineclub.ensulm), [instagram](https://www.instagram.com/cineclubens/) ou via la [newsletter](https://lists.ens.psl.eu/wws/info/cineclub-informations)

Cette rentrée, l'entrée est *gratuite* pour les membres du COF.

Financé par la CVEC du Crous de Paris et par PSL via les AIE
